﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompetitionInvites.Models;

namespace CompetitionInvites.Controllers
{
    /// <summary>
    /// The HomeController class will server as the main controller to execute methods when certain URLs are invoked
    /// </summary>
    public class HomeController : Controller
    {
        // GET: Home
        /// <summary>
        /// The method which will be called when either the home/index url is invoked or when the base website URL is called
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour; //gets the current time by hour only
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon"; //if the current time is before 12, say good morning, else say good afternoon
            return View(); //return the view found in Home/Index
        }
        /// <summary>
        /// Method which will be called when the 'RSVP' url is invoked.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult RSVP()
        {
            return View();//return the view found in Home/RSVP
        }
        /// <summary>
        /// Method which will be called when a form with a RSVP submit is called. It will take the 
        /// competitor object created in the form.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        [HttpPost]//use post so that the information passing between the two pages are hidden
        public ViewResult RSVP(CompetitorResponse response, string responseBtn)
        {
            if (ModelState.IsValid) //validate whether all the information the user entered is functionally correct
            {
                if (responseBtn == "I will attend")
                {
                    response.WillAttend = true; //set the boolean of the user's attending status to true if the user selected the 'I will attend' option
                }
                else
                {
                    response.WillAttend = false;//set the boolean of the user's attending status to false if the user selected 'I cannot attend'
                }
                return View("Thanks", response); //return a view which thanks the user for either being able to attend or regards of them not attending
            }
            else //the information was entered falsely, return the user back to the RSVP form
            {
                return View();
            }
            

        }
    }
}