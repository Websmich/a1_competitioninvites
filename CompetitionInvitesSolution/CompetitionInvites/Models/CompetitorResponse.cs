﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CompetitionInvites.Models
{
    /// <summary>
    /// Enumeration for the CompetitorResponse model which will allow the user to define what type of interest they have when attending the event
    /// </summary>
    public enum TechnicalInterest{
        [Display(Name = "Internet of Things")]
        Iot,
        Cognitive,
        Wearable,
        AR_VR

    }

    /// <summary>
    /// The CompetitorResponse class which will serve as a model for a competitor that will be attending the
    /// competition. The competitor can have:
    ///     A Name: string
    ///     An Email: string
    ///     A Phone #: string
    ///     Attending Status: Optional bool
    /// </summary>
    public class CompetitorResponse
    {
        /// <summary>
        /// The getter and setter method for the competitors name
        /// </summary>
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        /// <summary>
        /// The getter and setter method for the competitors email
        /// </summary>
        [Required(ErrorMessage = "Please enter your email address")]
        [RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        /// <summary>
        /// The getter and setter method for the phone number of the competitor
        /// </summary>
        [Required(ErrorMessage = "Please enter your phone number")]
        public string Phone { get; set; }

        /// <summary>
        /// The getter and setter method for the status of the competitor attending
        /// An optional variable is so that a null value can be returned. A null value can be represented as
        /// a "Not sure" response so that the user does not have to decide right away
        /// </summary>

        public bool? WillAttend { get; set; }

        /// <summary>
        /// The getter and setter method for the address provided by the user
        /// </summary>
        [Required(ErrorMessage = "Please specifiy your address")]
        public string Address { get; set; }

        /// <summary>
        /// The getter and setter method for the Twitter account name the user may or may not provide.
        /// It will not be required, as not everyone owns a twitter account or may wish to keep theirs private
        /// </summary>
        public string TwitterAccount { get; set; }

        /// <summary>
        /// The getter and setter method for the interest the user has while attending the event
        /// </summary>
        [Required(ErrorMessage = "Please specifiy your technical interest")]
        public TechnicalInterest Interest { get; set; }
    }
}